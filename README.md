# IF.SVNAdmin

#### 介绍
基于原IF.SVNAdmin 1.6.2版本修改。
修改功能如下：<p>1、原版本依赖Apache，修改版本无需依赖。</p><p>2、去除 IF.SVNAdmin 密码加密</p><p>3、创建仓库时，基于设定好的svnserve.conf模板文件</p>
